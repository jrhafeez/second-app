//
//  ViewController.swift
//  SecondApp
//
//  Created by cl-macmini-45 on 17/01/19.
//  Copyright © 2019 cl-macmini-45. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var viewControllerTextField: UITextView!
    
    @IBAction func viewControllerSendButton(_ sender: Any) {
        print("hi")
        let viewControllerNavigation = self.storyboard?.instantiateViewController(withIdentifier: "ReceivingViewController") as? ReceivingViewController
        viewControllerNavigation!.textFieldString = viewControllerTextField.text
        self.present(viewControllerNavigation!, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}

extension ViewController: SendDataBack {
    func sendMessageBack(message: String) {
        viewControllerTextField.text = message
    }
}


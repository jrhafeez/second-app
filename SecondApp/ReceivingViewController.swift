//
//  ReceivingViewController.swift
//  SecondApp
//
//  Created by cl-macmini-45 on 25/02/19.
//  Copyright © 2019 cl-macmini-45. All rights reserved.
//

import UIKit

protocol SendDataBack {
    func sendMessageBack(message: String)
}

class ReceivingViewController: UIViewController {
    
    var textFieldString: String?
    var delegate: SendDataBack?

    @IBOutlet weak var recivingControllerTextField: UITextView!
    
    @IBAction func recivingControllerSendButton(_ sender: Any) {
        delegate?.sendMessageBack(message: recivingControllerTextField.text ?? "Text Field is empty.")
        dismiss(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        recivingControllerTextField.text = textFieldString
    }
    
}
